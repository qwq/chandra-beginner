# Data analysis walkthrough for Abell 478 (Part 5)

_2019-06-24_

This note describes making exposure-corrected images.

The exposure map is a measure of effective exposure time across the detector.
It is a photon flux weighted map of the energy-dependent detector response
(how many data counts per photon). Therefore, if you make an exposure map in
an interval of energy, the spectral shape of the incoming photons will affect
the relative weights in different parts of the detector. Those model
parameters are used to compute what the expected energy spectrum is, in order
to weigh the counts image correctly.

https://space.mit.edu/ASC/docs/expmap_intro.pdf



After fitting a cluster average spectrum in Xspec, update `A478.json` with
those values for `"t"`, `"abund"`, `"z"`, `"nh"` from the `apec * tbabs`
model. These are needed to compute the exposure maps.

Open the coadded counts image and draw a rectangular region that will be the
field of view of the processed images. You can make images of a cropped
region, but for your first image, make this cover most/all of the available
data. Save this region file. Now update the `"reg_crop"` key of `A478.json` to
point to this region file.

Initialize HEASOFT and CIAO in the `A478/` folder, start IPython, then read in
the current `A478.json` content.

```python
import acisgenie
import acisgenie.genie
import acisgenie.image
A478info = acisgenie.genie.loadInfoJSON('A478.json')
acisgenie.image.fluximage(A478info, [[800, 2000]], ['0820'], 'mkimg0820')
```

The second argument `[[800, 2000]]` is to be a list of `[energy_low,
energy_hi]`, in units of eV.

The third argument `['0820']` is to be a list of `'filename'`. I typically
name files this way, so 0.8-4 keV is `0840`, 2-7 keV would be `2070` etc.

The fourth argument is to be the name of the bash script that will be
created/overwritten in the `A478/` folder.

The 2nd and 3rd arguments are lists because I originally used this to generate
commands that will process multiple narrow band images and write them to a
single bash script. But now I prefer making a separate bash script for each
narrow band image for better visibility. Hence the redundant (but necessary)
additional enclosure by `[]`.

> This routine does not process the exposure maps by itself, but writes
> commands that will do that to the output file (`'mkimg0820'` above). It will
> not warn you if certain files are missing (notably `chipmap.par` and the
> crop region file, both of which I often forget).

> The bash script must be run in a properly initialized environment. It needs
> all of HEASOFT, CIAO and CHAV. If you are missing any files, the bash script
> will still run all the way to the end but will throw a lot of errors and
> warnings, and nothing useful will be generated.

After a successful processing, the products will be in `images/`. In the
example above, there will be several files with the prefix `'0820'`.

* 0820.img is the counts image.
* 0820.ro is the rescaled readout artifact image.
* 0820.bg is the rescaled acis background image.
* 0820.exp is the exposure map in units of [s].
* 0820.flux is the exposure corrected background subtracted count rate image.

The units of the flux image is a little tricky, so be very clear about this.
It shows you

[FLUX] = COUNTS / S / PIXEL

The software pixel size of ACIS is 0.492", or 0.242064 square-arcsecond per
pixel. Use this information if you need to calculate [FLUX] in units of COUNTS
/ S / ARCSEC^2.

> If you bin the image, think very carefully about how it changes the meaning
> of values in the image.

You will also find imaging products in `image/` subdirectories in each obsid
included in `A478.json`. These are useful for diagnosing problems, or if you
want to selectively include a different combination of ObsIDs in the final
mosaic. When I have obtained the final mosaic for my needs, I delete these
constituent imaging products because they take up disk space. It can take a
while to regenerate them, don't delete anything until you are sure you won't
need them.

It is instructive to read the contents of `mkimg0820` to see what has been
done. The first part of the script iterates through every cleaned event file,
readoutbg, and ACIS background events file to create count images; rescale the
background images; compute the exposure maps for each. Then these are coadded
together, and the `*.flux` image is created. You should familiarize yourself
with this final part because you will do this by hand when combining images,
either of different ObsIDs or of different energy bands.

The calculation is the following (but this is not everything, you would need
all the details that the script has).

FLUX = BACKGROUND SUBTRACTED COUNTS / EXPOSURE MAP

BACKGROUND SUBTRACTED COUNTS = COUNTS - RESCALED READOUT - RESCALED ACISBG

To combine different sets of images, in the same energy band:

FLUX = SUM[BACKGROUND SUBTRACTED COUNTS] / SUM[EXPOSURE MAP]

To add flux images for different energy bands, they must be from the same
obsids and have the exact same CCDs, crop, e.t.c.:

FLUX = SUM[FLUX]


---
