# Data analysis walkthrough for Abell 478 (Part 3)

_2019-06-24_

The steps below will get you to extract a spectrum of the cluster.


## 1. Set up

Obtain a copy of my Chandra data analysis tools and run it in Python in
interactive mode (preferably IPython). First clone the repository to your
`~/astro/soft` folder, then go into the folder and checkout the `dev` branch.

```bash
cd ~/astro/soft
git clone https://gitlab.com/qwq/astro-galaxyclusters-chandra.git
mv astro-galaxyclusters-chandra acisgenie
cd acisgenie
git checkout dev
```

Stay with with `dev` unless something breaks, because branch `master` is
updated with changes from `dev` infrequently. To update your local `dev`
branch, use `git pull` while in the local folder. If something is broken,
check the project page to see if there is a newer version of `master` or
revert back to an older version of `dev`.

You can see a summary of commit history in the current branch with `git log`.


Add this location (`$HOME/astro/soft/acisgenie`) to your `PYTHONPATH`. If you
already have a line for this in your `.bashrc` file, prepend to it. Otherwise
add a new line there,

```bash
export PYTHONPATH=$HOME/astro/soft/acisgenie:$PYTHONPATH
```

There are some executable scripts in `acisgenie/bin` and `acisgenie/utils`,
but these might not work right out of the box on your system. They all call
for the executable `#!/usr/bin/env python3`, which resolves to what your
system has for `which python3`. If you need anything there, link/copy what you
need to one of your `PATH` folders and modify as necessary. Or if you wish you
can add the `bin/` folder to your `PATH`.


To use the acisgenie scripts, you must initialize the environment for HEASOFT
and CIAO, and in the same session, run IPython. Python inherits the
environmental variables from your shell.

> These scripts use CIAO and ZHTOOLS programs so they only work in a properly
> initialized environment.

Start IPython,

```
└─ $ ipython
Python 3.5.4 (default, Sep 14 2018, 15:42:52)
Type 'copyright', 'credits' or 'license' for more information
IPython 6.5.0 -- An enhanced Interactive Python. Type '?' for help.
```

then run the following `import` statements. You can copy them here and type
`%paste` in IPython.

```python
import acisgenie
import acisgenie.genie
import acisgenie.spec
import acisgenie.image
import acisgenie.acisbg
import acisgenie.readoutbg
```

You can read the docstring of any function by printing the `__doc__`
attribute, e.g.

```python
print(acisgenie.spec.extract_spectrum.__doc__)
```

Some functions have useful docstrings, some don't. 

Your can quit IPython by pressing `Ctrl + D` twice when there is nothing
entered in the command line. You can clear the current command with `Ctrl +
C`.


---


## 2. Create a JSON file containing context about the data


The spectral extraction and exposure map image routines need context
information for the ObsIDs, namely what files contain necessary information
such as cleaned event files, ACIS background files, and lists of CCDs to use.
These need to be in a JSON (JavaScript Object Notation) file. A starting file
for A478 is attached, with the details for event files and acisbg files
already filled out.

Edit the JSON file as a text file. Information for the JSON syntax is here
(https://www.json.org/). Before raising a problem make sure your file passes
the linter here (https://jsonlint.com/).

> There are comments at the top of the JSON file to remind you what some of
> the properties are.

In the JSON file, `"dirname"` and `"target"` are not used for any processing,
but are there to "remind you what object the JSON file is for.



### 2.1 Partially fill JSON file with sufficient context to extract spectra


> Not all properties in the JSON file are used for spectral extraction. Some
> are only used for imaging. They have no effect in this part.

First fill `"obs":[]` with information on the processed files for all ObsIDs.
`[]` are arrays. `"obs"` is an array of `{}` (JSON objects). Syntactically it
will look like this:

```json
"obs": [
    {}, {}, {}
    ]
```

where there is one `{}` for each ObsID, separated by comma.

A JSON array is a comma-separated list of variables. A JSON object comprises
of a comma-separated list of `key:value` pairs. Keys are **always
double-quoted** strings `"likethis"`, while values can be any valid variable.

> **JSON does not accept single quotes**. The linter will not parse them. Use
> only double quotes for quoting.

The obsid properties are mostly self-explanatory following a pattern. You need
to modify them to the appropriate values for each ObsID. Some things to note:


1. `"chips":[]` is an array of `ccd_id` that `spec.extract_spectrum` uses. It
   will only process the CCDs listed here, even if there is data for other
   CCDs in your files.

2. `"evtfiles": [{}, {}, ...]` is an array of objects. Each object holds
   information about a cleaned event file. E.g. 6102 is an ACIS-I pointing
   with just one `evt3.fits` file. 1669 has `evt3.fits` and `evt3_BI.fits` so there
   are two objects for it.

3. `"bgfiles": [{}, {}, ...]` is an array of objects too. Each objects holds
   information about an acis background events file. The CCD grouping of
   bgfiles need not be the same as evtfiles. However, for all CCDs covered by
   a cleaned event file, one of the acisbg files must also cover that CCD, or
   there will be an error!

4. Each `evtfile` has a property `"rorescale"`. This is a rescaling factor for
   the readout background, to be used for imaging analysis. There's a line in
   the comments to remind you what this should be. To calculate this value,
   there is a convenience function
```
acisgenie.readoutbg.get_readout_rescale(evtfile)
```
that prints the **inverse** of this value. Take the reciprocal of this for
`rorescale`. You can use `get_readout_rescale` on any cleaned event file that
has the correct header (evt1, evt2, evt3 are all OK, there is only one value
of `rorescale` for each obsid).

5. Each bgfile has a property `"ctrescale"`. This is a rescaling factor for
   the ACIS background for imaging. There's also a line in the comments to
   remind you what this should be. To calculate this value, there is a
   convenience function
```
acisgenie.acisbg.calc_bgimg_rescale(evtfile, bgfile, ccd=None)
```
where `evtfile`, `bgfile`, and `ccd` must be consistent, e.g. for 1669's chip
5, use `'evt3_BI.fits'`, `'acisbg_5.fits'`, `ccd=[5]`; for chip 7, use
`'evt3_BI.fits'`, `'acisbg_7.fits'`, `ccd=[7]`; for chips 236, use
`'evt3.fits'`, `'acisbg_236.fits'`, `ccd=[2,3,6]`.


Try calculating `rorescale` and `ctrescale` and verify that you get the same
values as in the JSON file attached.

> When you are done, run the content of the JSON file through jsonlint before
> continuing.


---


## 3. Extract a spectrum for the cluster

### 3.1 Requirements

The following must be taken care of before you can extract spectra. It needs
to be done only once, unless your processed `evt`, `readoutbg` or `clean.gti`
files change. If so, these actions must be repeated.

1. In the JSON file, ensure the property `"specparfile":"spec.par"` points to
   a valid file for `spec.par`.

2. Run the following processing on all `readoutbg` files, to append a fake GTI
   extension. **This only needs to be done once for each readoutbg file.**
   E.g. `evt3.fits` goes with `readoutbg.fits` and `evt3_BI.fits` goes with
   `readoutbg_BI.fits`. `verbose=1` will print informational message about the
   exposure times so you can see it worked.
```
acisgenie.readoutbg.readout_append_gti(evtfile, rdtfile, verbose=1)
```
> If you remake the `readoutbg` file or update it with new `clean.gti`, the
> old fake GTI extension is no good and you must do this once more.
3. You must have prepared a region file with all the point sources as positive
   regions.


### 3.2 Extract spectrum from a given region

Now you are ready to extract spectra. Extract a spectrum from an area covering
most of the cluster, out to a radius of between 2 and 4 arcminutes. Usually 3
arcminutes work. This procedure can be repeated for any region you want to
extract a spectrum from. For organization, I place all region files in `reg/`,
and extracted spectra in folders with their region names in `spec/`. If you do
something different, you should use the appropriate relative file path for
your files.


1. Load the coadded image in DS9, and select a region to extract spectrum
   from. Save the region as a DS9 region file with fk5 coordinate system. The
   example here uses `c3.reg`, which is a 3 arcminute radius circle centered
   on A478.

2. Initialize HEASOFT and CIAO, run IPython, and import the acisgenie modules.

3. Go to the top level directory
```
cd ~/astro/cluster/A478
```
4. Read the data from `A478.json` into a variable
```
A478info = acisgenie.genie.loadInfoJSON('A478.json')
```
5. Extract spectra, naming the output files with the prefix `'c3'`
```
acisgenie.spec.extract_spectrum('c3', 'reg/c3.reg', A478info, exclregfile='reg/pts.reg')
```

Wait a little bit for it to finish. A bunch of things are taken care of by the
routine here: a spectrum is extracted from each CCD that has at least 10
counts in the specified region; they are then added together for each ObsID,
before added together across ObsIDs. The ARF and RMF files are weighed by
0.5-2 keV counts and added. The ACIS background files are added, and an
adjusted `BACKSCAL` value calculated and written to the final `c3.pi` file.
You can view the procedure in `acisgenie/spec.py`, in `extract_spectrum()`.
This procedure is something I learned through doing it manually before
evolving into this semi-automated process.

When it is done, these new files appear in the top level directory:

```
-rw-rw-r--  1 qw  753 addarf_c3_6102.arf.log
-rw-rw-r--  1 qw  321 addarf_c3.arf.log
-rw-rw-r--  1 qw 2.2K addrmf_c3_6102.rmf.log
-rw-rw-r--  1 qw 1.3K addrmf_c3.rmf.log
-rw-rw-r--  1 qw  43K c3_1669.arf
-rw-rw-r--  1 qw  60K c3_1669.bg
-rw-rw-r--  1 qw  60K c3_1669.pi
-rw-rw-r--  1 qw 4.6M c3_1669.rmf
-rw-rw-r--  1 qw  60K c3_1669.ro
-rw-rw-r--  1 qw  43K c3_6102.arf
-rw-rw-r--  1 qw  17K c3_6102.bg
-rw-rw-r--  1 qw  17K c3_6102.pi
-rw-rw-r--  1 qw 6.4M c3_6102.rmf
-rw-rw-r--  1 qw  17K c3_6102.ro
-rw-rw-r--  1 qw  43K c3.arf
-rw-rw-r--  1 qw  17K c3.bg
-rw-rw-r--  1 qw  17K c3.pi
-rw-rw-r--  1 qw 6.4M c3.rmf
-rw-rw-r--  1 qw  17K c3.ro
```

Move them into a different folder for better organization.

```bash
mkdir -p spec/c3
mv c3* spec/c3/
mv *c3*.log spec/c3/
```


---


