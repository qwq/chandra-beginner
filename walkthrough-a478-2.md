# Data analysis walkthrough for Abell 478 (Part 2)

> Before you start, initialize the CIAO environment. You also need the
> binaries from ZHTOOLS, CHAV and ACISBG to be accessible from your `$PATH`.
> In addition, you also need the script `bpix_fits_to_txt`. Save them to a
> folder that is in your `$PATH`, and check that they can be found before
> starting.

You previously reprocessed the archival data files to Level=2 events, then
checked the light curve for background flares and filtered the Level=2
products using the flare-filtered GTI file. In this part, you will produce an
appropriate ACIS blank sky background dataset for the observation. These are
frequently referred to as "acisbg", and we will name the files that way too.

First of all, there is a CIAO thread about this here. It describes how to look
up the relevant files in CALDB and how to work with the files within the CIAO
ecosystem. Read through the guide because it has links to other memos that
describe the ACIS background in more detail.

http://cxc.harvard.edu/ciao/threads/acisbackground/

The links are in the related links part at the top of the page. This one in
particular you should try to understand. It describes properties of the ACIS
background

http://cxc.harvard.edu/contrib/maxim/bg/

In practice you need to do these steps:

1. Identify the ACIS background epoch of the observation, e.g. A-G. Find the
   relevant background files in CALDB.

2. Decide which CCDs you will use for your imaging and spectral analysis. Look
   up the exposure time info from the GTI extension of the background files.
   Merge the files that have the same exposure time so that you can process
   them together and save time.

3. Filter using `[status=0]` if VFAINT, skip this step if FAINT.

4. Copy the file over to the `processed/` folder, set up `make_acisbg.par`,
   and run `make_acisbg` to recalculate the `sky(x,y)` values of the events
   using the pointing info from `evt3.fits`. Then apply the same bad pixel
   mask to the resulting `acisbg.fits`.

5. Calculate the rescaling ratio for the ACIS background.

This procedure is described in [Hickox & Markevitch
(2006)](https://iopscience.iop.org/article/10.1086/504070/pdf).

Note that Hickox & Markevitch used the stowed background in order to study the
difference of stowed vs. blank sky: the CXB (cosmic X-ray background, from all
the unresolved sources). If we use the blank sky background, we subtract both
the instrument background and CXB in order to derive what remains: the
extended emission from the ICM.


## Procedure for deriving the ACIS background


### 0. Obtain the ACIS Blank Sky data

> Skip this step if you already have them in your CALDB.

Get the ACIS blank sky background files. Go to your CALDB folder and download
the package:

```
wget ftp://cda.harvard.edu/pub/arcftp/caldb/acis_bkgrnd_4.7.6.tar.gz
```

If you followed previous instructions for CIAO installation, CALDB will be at
`$HOME/astro/soft/CALDB`. You would also have an environment variable `$CALDB`
that has the path to it. If you went with default CIAO settings, there is
instead a CALDB folder in the ciao installation. Make sure you are in the
CALDB folder (it has 3 folders, `data`, `software`, `docs`). Extract the
package there.

```
tar xf acis_bkgrnd_4.7.6.tar.gz
```

Listing files in `$CALDB/data/chandra/acis/bkgrnd` should show you many FITS
files.


### 1. Identify the ACIS background epoch of the observation.

Example for 1669:

```
└─ $ acis_bkgrnd_lookup evt3.fits
/usr/local/ciao/ciao-4.11/CALDB/data/chandra/acis/bkgrnd/acis2sD2000-12-01bkgrnd_ctiN0006.fits
/usr/local/ciao/ciao-4.11/CALDB/data/chandra/acis/bkgrnd/acis3sD2000-12-01bkgrnd_ctiN0006.fits
/usr/local/ciao/ciao-4.11/CALDB/data/chandra/acis/bkgrnd/acis6sD2000-12-01bkgrnd_ctiN0006.fits
/usr/local/ciao/ciao-4.11/CALDB/data/chandra/acis/bkgrnd/acis8sD2000-12-01bkgrnd_ctiN0003.fits
```


There are BI chips, so look those up as well.

```
└─ $ acis_bkgrnd_lookup evt3_BI.fits
/usr/local/ciao/ciao-4.11/CALDB/data/chandra/acis/bkgrnd/acis5sD2000-12-01bkgrnd_ctiN0002.fits
/usr/local/ciao/ciao-4.11/CALDB/data/chandra/acis/bkgrnd/acis7sD2000-12-01bkgrnd_ctiN0002.fits
```

This tool tells you which ACIS background file is appropriate, based on 1) the
date of the observation, and 2) the CCDs contained in the event file. The
paths shown are based on the CALDB path that CIAO remembers. The background
files may or may not be there (you can run this command without having
downloaded the ACIS blank sky data set). What we need is this info:

```
acis2sD2000-12-01bkgrnd_ctiN0006.fits
acis3sD2000-12-01bkgrnd_ctiN0006.fits
acis6sD2000-12-01bkgrnd_ctiN0006.fits
acis8sD2000-12-01bkgrnd_ctiN0003.fits
```

The first part of the file name is always `acis`, followed by the CCD ID, then
`i` or `s` depending on the pointing, then `D` followed by a date
`YYYY-MM-DD`. This date is the beginning of the validity period for this
background. `_cti` means this background file has been corrected for CTI, and
`Nxxxx` is the processing version of this file. Every time a new processing
runs, the number goes up by one.

The period beginning 2000-12-01 is period "D", beginning 2005-09-01 is period
"E", beginning 2009-09-21 is period "F", beginning 2012-01-01 is period "G".


Do the same for 6102.

(You'll find that it is also period D, five files for each of 01236, but these
are acis**i**, not acis**s**, files! Not the same ones as for 1669.)



### 2. Merge ACIS background files that have the same exposure

First thing to do is find out the effective exposure time of each file. They
have the usual format for event files:

```
└─ $ dmlist acis2sD2000-12-01bkgrnd_ctiN0006.fits blocks

--------------------------------------------------------------------------------
Dataset: acis2sD2000-12-01bkgrnd_ctiN0006.fits
--------------------------------------------------------------------------------

     Block Name                          Type         Dimensions
--------------------------------------------------------------------------------
Block    1: PRIMARY                        Null       
Block    2: EVENTS                         Table        11 cols x 190134   rows
Block    3: GTI2                           Table         2 cols x 1        rows
```

So the third (index 0, 1, 2, so +2) extension is the GTI one.


For the background files for 1669, we have these:

```
└─ $ gtiexp acis2sD2000-12-01bkgrnd_ctiN0006.fits+2                
   500000.000

└─ $ gtiexp acis3sD2000-12-01bkgrnd_ctiN0006.fits+2
   500000.000 

└─ $ gtiexp acis6sD2000-12-01bkgrnd_ctiN0006.fits+2
   500000.000

└─ $ gtiexp acis8sD2000-12-01bkgrnd_ctiN0003.fits+2
   100000.000
```

So CCD 2, 3, 6 have the same exposure time and we can merge them together to
simplify analysis. CCD 8 has a much shorter exposure and need to be separately
analyzed.

```
└─ $ gtiexp acis5sD2000-12-01bkgrnd_ctiN0002.fits+2
   170000.000   

└─ $ gtiexp acis7sD2000-12-01bkgrnd_ctiN0002.fits+2
   450000.000 
```

The BI chips also have different exposures.

Thus, altogether, you will need to renormalize 4 different acisbg files:

236; 8; 5; 7


This is what we will do for 236:

```
└─ $ dmmerge "acis2sD2000-12-01bkgrnd_ctiN0006.fits,acis3sD2000-12-01bkgrnd_ctiN0006.fits,acis6sD2000-12-01bkgrnd_ctiN0006.fits" aciss_D_236_bg_evt.fits
warning: DETNAM has different value...Merged...
```

I named it `aciss_` to tell us that it is an ACIS-S pointing; `D_` to tell us
it is for period "D", `236_` to tell us there are ccds 2, 3, 6 in there, and
`bg_evt` to tell us this is a background events file.

The warning about `DETNAM` is due to each of the 3 files having different
values for this. We do want to update `DETNAM` so that it says `ACIS-236`.
This ensures that this header keyword remains consistent with the data
contained in the FITS file.

```
dmhedit aciss_D_236_bg_evt.fits none add DETNAM 'ACIS-236             / Detector'
```

Next we need to make sure there is a GTI extension in this merged file. Take a
look at the file just created, using `dmlist`. It's gone! It doesn't matter
which of the 3 files to get this GTI extension from; they have the same
exposure time.

Use `fappend` to copy the `+2` extension of one of the original 3 to the new
file.

```
fappend acis2sD2000-12-01bkgrnd_ctiN0006.fits+2 aciss_D_236_bg_evt.fits
```

Now it has a valid GTI extension, and you can look up its exposure time:

```
└─ $ gtiexp aciss_D_236_bg_evt.fits+2
   500000.000 
```


For the background files for 6102, all of the chips `01236` have the same
exposure time (1.5 Ms) and can be merged together (you figure out how). Create
from them a merged file named `acisi_D_01236_bg_evt.fits` using the same
procedure as you did for `aciss_D_236_bg_evt.fits`.



### 3. Filter for VFAINT observation if needed

6102 was observed in VFAINT mode. The blank sky background files don't have
any other status flags set, other than the VFAINT flag. So a `status=0` filter
would extract only those that pass the VFAINT mode filter.

```
dmcopy acisi_D_01236_bg_evt.fits'[status=0]' acisi_D_01236_bg_evt_vf.fits
```

Here I named it with `_vf` to remind me that it has been VFAINT filtered. We
will use this `_vf` version for 6102.

> Again, check whether the GTI extension stuck! If not, you must use fappend
> to copy the GTI extension to the new `_vf.fits` file.




### 4. Copy these prepared files to the analysis folder

Copy the files to the `processed/` folder in each obsid (carefully selecting
the correct files!) Just use `cp` to this.

Use the following names for the copied files.

In 1669, there are 4 different files. Name them like this:

```
acbg_236.fits
acbg_8.fits
acbg_5.fits
acbg_7.fits
```

In 6102, there is just 1 file. Name them like this:

```
acbg_01236.fits
```

For each of these files, you need to run `make_acisbg` with an appropriate
parameter file (which I name variations of `make_acisbg.par`). Here is an
example for 1669. You should figure out the others.


#### ObsID 1669, CCDs 236:

Create a par file named `make_acisbg_236.par` in `1669/processed/`. Its
content:

```
## make_acisbg.par
## A leading space comments the line out; all lines except in the form
## name=value are ignored.
evt_file=evt3.fits
aoff1_file=aoff1.fits
gti_file=clean.gti
bg_file=acbg_236.fits             - this file will be modified
```

Now run in the `processed/` folder:

```
make_acisbg @make_acisbg_236.par
```

It will modify `acbg_236.fits` to update the sky coordinates of the events using
information from `evt3.fits`.

For the next step, you need the attached utility script `bpix_fits_to_txt`
(it's a Python program). It converts the bad pixel file in FITS format to a
text file that `badpixfilter` from CHAV understands. Put it somewhere in the
`$PATH` and make it executable (`chmod +x`).

```
bpix_fits_to_txt ../secondary/1669_bpix1.fits ../secondary/1669_bpix1.txt

badpixfilter evtfile=acbg_236.fits o=acisbg_236.fits -badpixfilter ../secondary/1669_bpix1.txt
```

You have now created `acisbg_236.fits`, which is the final ACIS background
file product for ccds 2, 3, and 6. You can now remove `acbg_236.fits`. If you
find that something isn't right with `acisbg_236.fits` (say, you made a
mistake) you should create it from the beginning again.


#### ObsID 1669, CCD 7:

For the BI, chip, use the correct files in `make_acisbg.par`. Create another
par file named `make_acisbg_7.par` in `1669/processed`. Its content:

```
## make_acisbg.par
## A leading space comments the line out; all lines except in the form
## name=value are ignored.
evt_file=evt3_BI.fits
aoff1_file=aoff1_BI.fits
gti_file=clean_BI.gti
bg_file=acbg_7.fits             - this file will be modified
```

The difference is that all the cleaned files have `_BI` in the names, since
those are the ones that have CCD 7 in them. The aspect information may not be
the same as the FI chip files, because the flare cleaning may have produced
different GTI times.

> ***Only CCDs 5 and 7 are BI chips.***

```
make_acisbg @make_acisbg_7.par
```

This modifies `acbg_7.fits`. You do not need to run `bpix_fits_to_txt` again
for 1669, it needs to be done just once for each obsid. The same bad pixel
text file has the data for all CCDs.

```
badpixfilter evtfile=acbg_7.fits o=acisbg_7.fits -badpixfilter ../secondary/1669_bpix1.txt
```

You now have `acisbg_7.fits`. You can remove `acbg_7.fits`.


#### ObsID 1669, the other CCDs (5 and 8)

Repeat the procedure for `acbg_8.fits`, `acbg_5.fits` to create
`acisbg_8.fits` and `acisbg_5.fits`. Then, do the same for ObsID 6102,
`acbg_01236.fits` -> `acisbg_01236.fits`.



### 5. Calculate renormalization factors for the ACIS background data

This will be done later, just before doing imaging and spectral analysis.
