# Installing Miniconda and Python 3

> Setting up Python through conda makes it easy for the user to control their
> Python environment(s). Each environment can be independently managed with
> their own versions and packages, and you also won't be affected by any
> changes to the system Python when your OS is updated.

Follow the instructions at https://docs.conda.io/en/latest/miniconda.html. The steps below sets up Miniconda 3 on a Ubuntu machine, installing it in the default location in the user's home directory.

```
cd ~
wget https://repo.anaconda.com/miniconda/Miniconda3-latest-Linux-x86_64.sh
bash Miniconda3-latest-Linux-x86_64.sh
```

Accept the default location and agree to terms. When done, close the window
and open a new terminal for the modified `.bashrc` to take effect.

To check if Miniconda set up successfully, start a new terminal and see if
`conda` can be found. If not, run the executable using the full path and have
it set up the shell rc file again, after which a new terminal should be
launched for it to take effect.

```
$HOME/miniconda3/bin/conda init
```

## Create a new Python environment with `conda`

Now we need to create a new Python 3.5 environment (to be the same version as
included in CIAO 4.11) that we can use instead of CIAO's. We will set up CIAO
to use the Python executables from the Miniconda installation, while making
CIAO's Python packages available for use.

Let's name this new environment `ciao4.11`, with the intention that this
environment will only be used for CIAO 4.11, in case you use a different
version of CIAO in the future.

```
conda create -n ciao4.11 python=3.5 pip ipython astropy
```

This will have Python 3.5 installed, together with the packages `pip`,
`ipython` and `astropy`.

To activate the environment,

```
conda activate ciao4.11
```

When conda is active but no user environment has been activated, there will be
`(base)` at the beginning of your shell command prompt indicating this. When
the `ciao4.11` environment is activated, there will be `(ciao4.11)` at the
beginning of your shell command prompt to indicated it. To deactivate,

```
conda deactivate
```

## Override default Python executables for CIAO

Add the Python and IPython executables from the Miniconda installation to
`~/.ciaorc` to use them instead of the ones included with CIAO.

First activate the `ciao4.11` environment, then ask `which python`, e.g.

```
$ which python
/home/qw/miniconda3/envs/ciao4.11/bin/python
$ which ipython
/home/qw/miniconda3/envs/ciao4.11/bin/ipython
```

Then open the `.ciaorc` file,

```
vim ~/.ciaorc
```

Look for the line `#PYTHON CIAO`, add this line below it

```
PYTHON /home/qw/miniconda3/envs/ciao4.11/bin
```

Then look for the line `#IPYTHON CIAO`, and add this line below it

```
IPYTHON /home/qw/miniconda3/envs/ciao4.11/bin
```

> You should use the full path in the above, i.e. without `~` or `$HOME`.

Then open a new terminal and try this (in this order)

```
conda activate ciao4.11
heainit
ciao
```

---
