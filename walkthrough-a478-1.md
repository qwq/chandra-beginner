# Data analysis walkthrough for Abell 478 (Part 1)

> Before you start, initialize the CIAO environment. You also need the
> binaries from CHAV and ZHTOOLS to be accessible from your `$PATH`. In
> addition, you need the scripts `make_readout_bg` and `plot_lc`. They are
> attached. Save them to a folder that is in your `$PATH`, and check that they
> can be found before starting.

Follow the steps in this note for a demonstration of the data reduction
process to reprocess archival event files. We will produce what are known as
"Level=2 event files" from the archival "Level=1 event files". We then examine
the light curve to filter any background flares.

You should read more about what each CIAO tool does on the CXC website.


I attach some of my files for you to use, and those can be your templates for
other observations. Remember to check the content of the tar.gz file before
unpacking it. The command is `tar tf A478.tar.gz`. To extract everything into
the current working directory, it is `tar xvf A478.tar.gz`.


## 1. Look up and download archival data


This is the place to begin. Go to the Chandra Web Chaser website.

https://cda.harvard.edu/chaser/mainEntry.do

In the search form, fill the name "Abell 478" and **click "Resolve"**. Select
Instrument: ACIS, Grating: None, Exposure Mode: TE (Timed Exposure), and Row
Limit: No limit.

> If you do not "Resolve" the name, then the search is only performed based on
> target name as written in the proposals, instead of a search based on
> position on the sky. The results will very likely be incomplete.

We will use two observations of A478 for this purpose. ObsID 1669 is a 42 ks
ACIS-S exposure taken in FAINT mode, while ObsID 6102 is a 10 ks long ACIS-I
exposure taken in VFAINT mode.


It is important to keep an organized directory structure so that the archival
files, your data reduction products, and files from further analysis are not
mixed up in the same folder. For this walkthrough, you must follow the
instructions exactly, because most of the commands use file paths relative to
the current working directory.

Create a folder for this cluster:

```
mkdir -p $HOME/astro/cluster/A478

cd $HOME/astro/cluster/A478
```

The `-p` flag for `mkdir` tells it to create subdirectories as needed. Without
it, if the directory `cluster` in the above example did not exist, then none
of its subdirectories can be created.

We will refer to this directory, `A478/`, as the top-level directory.

In `A478/`, run `download_chandra_obsid` to get the archival data:

```
download_chandra_obsid 1669,6102 evt1,asol,bpix,mtl,msk,pbk,stat,bias
```

This creates two folders `1669/` and `6102/`, and each has two folders in them
`primary/` and `secondary/`.


---


## 2. Process archival Level=1 event files to Level=2 products

### 2.1 Process ObsID 6102


When reprocessing archival event files, there is one thing to look out for:
whether the observation was taken with `FAINT` or `VFAINT` mode. `VFAINT` mode
stores an additional ring of pixels per event, giving more information that
can be used to classify an event as background. This is stored in the header
key `DATAMODE` of the events file, which you can check using dmkeypar:

```
└─ $ dmkeypar 6102/secondary/acisf06102_000N003_evt1.fits.gz DATAMODE echo+
VFAINT
```

Some of the commands below are split into multiple lines, each terminated with
the character `\`. Note in order for it to be correctly interpreted as a line
continuation, that there must not be any trailing space after `\`. Be careful
when copying and pasting commands.

These are the files in the `A478/6102/` after downloading them from the archive:

```
└─ $ ll 6102/*
6102/primary:
total 5296
drwxr-xr-x  4 qw   128B ./
drwxr-xr-x  4 qw   128B ../
-rw-r--r--  1 qw    22K acisf06102_000N003_bpix1.fits.gz
-rw-r--r--  1 qw   2.6M pcadf211482871N003_asol1.fits.gz

6102/secondary:
total 65320
drwxr-xr-x  13 qw   416B ./
drwxr-xr-x   4 qw   128B ../
-rw-r--r--   1 qw    29M acisf06102_000N003_evt1.fits.gz
-rw-r--r--   1 qw   5.1K acisf06102_000N003_msk1.fits.gz
-rw-r--r--   1 qw   459K acisf06102_000N003_mtl1.fits.gz
-rw-r--r--   1 qw   327K acisf06102_000N003_stat1.fits.gz
-rw-r--r--   1 qw   437K acisf211481317N003_1_bias0.fits.gz
-rw-r--r--   1 qw   432K acisf211481317N003_2_bias0.fits.gz
-rw-r--r--   1 qw   436K acisf211481317N003_3_bias0.fits.gz
-rw-r--r--   1 qw   430K acisf211481317N003_4_bias0.fits.gz
-rw-r--r--   1 qw   437K acisf211481317N003_5_bias0.fits.gz
-rw-r--r--   1 qw   3.9K acisf211482490N003_pbk0.fits.gz
-rw-r--r--   1 qw   499B badpix_framestore_01236.txt
```

Before you start, you need to add a text file `badpix_framestore_01236.txt`.
This is used to specify pixels affected by the framestore shadow to include
them in the bad pixel filter. You can use the template below. The affected
pixels are the same and only the ccd_id column varies. Add a row for every CCD
that was used in the observation. You can find this information in the
`DETNAM` keyword. I chose to name these files with the string of ccd_id as
suffix (it's an arbitrary choice).

```
└─ $ dmkeypar 6102/secondary/acisf06102_000N003_evt1.fits.gz DETNAM echo+
ACIS-01236
```

Content of `6102/secondary/badpix_framestore_01236.txt`:

```
#ccd_id chipx_lo chipx_hi chipy_lo chipy_hi time time_stop bit action
#------ -------- -------- -------- -------- ---- --------- --- ------
0       2       1023    2       9       0       0       1       include
1       2       1023    2       9       0       0       1       include
2       2       1023    2       9       0       0       1       include
3       2       1023    2       9       0       0       1       include
6       2       1023    2       9       0       0       1       include
```



#### Begin processing Level=1 to Level=2



Start in the directory `A478/`.

1. Reset existing status flags on the archival file.
```
acis_clear_status_bits 6102/secondary/acisf06102_000N003_evt1.fits.gz
```
2. Run destreak on the events.
```
destreak infile=6102/secondary/acisf06102_000N003_evt1.fits.gz \
    outfile=6102/secondary/evt1_destreak.fits mask=None filter=no
```
3. Store header information in a separate file for other CIAO tools, naming it
   `6102_evt1.par`.
```
dmmakepar 6102/secondary/evt1_destreak.fits \
    6102/secondary/6102_evt1.par clob+
```
4. Build a list of bias files in the `6102/secondary` folder.
```
cd 6102/secondary
ls -1 acisf*_?_bias*.fits* > bias.lis
cd ../..
```
5. Build a list of bad pixels, applying a custom `bitflag` option (this may
   differ from what you find in CIAO threads). In addition, a user specified
   file is supplied, containing rows affected by the frame store shadow on the
   outer edge of the CCDs.
```
acis_build_badpix obsfile=6102/secondary/6102_evt1.par \
    pbkfile=6102/secondary/acisf211482490N003_pbk0.fits.gz \
    biasfile=@6102/secondary/bias.lis \
    outfile=\!6102/secondary/abb1_bpix1.fits \
    usrfile=6102/secondary/badpix_framestore_01236.txt \
    bitflag=00000000000000011111100010012111 \
    calibfile=CALDB mode=h verbose=0
```
6. Run the afterglow pipeline to flag these events.
```
acis_find_afterglow infile=6102/secondary/evt1_destreak.fits \
    outfile=\!6102/secondary/aglow_bpix1.fits \
    badpixfile=6102/secondary/abb1_bpix1.fits \
    maskfile=6102/secondary/acisf06102_000N003_msk1.fits.gz \
    statfile=6102/secondary/acisf06102_000N003_stat1.fits.gz \
    mode=h verbose=0
```
7. Run the bad pixel pipeline again, adding the afterglow events identified,
   writing a second bad pixel file `aglow_bpix1.fits`.
```
acis_build_badpix obsfile=6102/secondary/6102_evt1.par \
    pbkfile=6102/secondary/acisf211482490N003_pbk0.fits.gz \
    biasfile=None outfile=\!6102/secondary/6102_bpix1.fits \
    bitflag=00000000000000011111100010012111 \
    calibfile=6102/secondary/aglow_bpix1.fits \
    mode=h verbose=0
```
8. Update the `BPIXFILE` value in the destreaked Level=1 events file header.
```
dmhedit 6102/secondary/evt1_destreak.fits none add BPIXFILE 6102_bpix1.fits
```
9. Update the `6102_evt1.par` file.
```
dmmakepar 6102/secondary/evt1_destreak.fits 6102/secondary/6102_evt1.par clob+
```
10. Run `acis_process_events` to apply the current calibration data, as well
    as the bad pixel files we just generated. **Note** the option
    `check_vf_pha=yes`. This must be set to `yes` for VFAINT mode, and set to
    `no` for FAINT mode observations.
```
acis_process_events infile=6102/secondary/evt1_destreak.fits \
    outfile=6102/secondary/6102_evt1.fits \
    badpixfile=6102/secondary/6102_bpix1.fits \
    acaofffile=6102/primary/pcadf211482871N003_asol1.fits.gz \
    mtlfile=6102/secondary/acisf06102_000N003_mtl1.fits.gz \
    eventdef=")stdlev1" \
    check_vf_pha=yes
```
11. Simulate the readout artifact.
```
make_readout_bg 6102/secondary/evt1_destreak.fits \
    6102/primary/pre_readout.fits \
    6102/primary/pcadf211482871N003_asol1.fits.gz \
    6102/secondary/6102_bpix1.fits 0 CALDB CALDB \
    6102/secondary/acisf06102_000N003_mtl1.fits.gz
```
12. Update headers.
```
r4_header_update 6102/secondary/6102_evt1.fits
r4_header_update 6102/primary/pre_readout.fits
```
13. Apply the standard event grade filter. The output files are Level=2
    events, and the simulated readout artifact (as a background).
```
dmcopy 6102/secondary/6102_evt1.fits"[EVENTS][grade=0,2,3,4,6,status=b00000000000000000000000000x00000]" \!6102/primary/6102_evt2.fits
dmcopy 6102/primary/pre_readout.fits"[EVENTS][grade=0,2,3,4,6,status=b00000000000000000000000000x00000]" \!6102/primary/6102_readoutbg.fits
```
14. Remove the column `phas` from Level=2 files. This reduces file sizes by
    getting rid of the event island images. They are not needed for further
    analysis. After removing them, you will not be able to run
    `acis_process_events` on these files, so if you need to redo these steps,
    you should begin from the very top.
```
dmcopy 6102/primary/6102_evt2.fits"[cols -phas]" \!6102/primary/6102_evt2.fits
dmcopy 6102/primary/6102_readoutbg.fits"[cols -phas]" \!6102/primary/6102_readoutbg.fits
```
15. Create auxiliary files for later analysis. Make region files bounding the
    field-of-view of the CCDs. This creates the region file
    `6102/primary/fov.reg` in physical coordinates. This region file is valid
    only when this particular ObsID is open (either events or image).
```
skyfov 6102/primary/6102_evt2.fits 6102/primary/fov.reg \
    aspect=6102/primary/pcadf211482871N003_asol1.fits.gz \
    mskfile=6102/secondary/acisf06102_000N003_msk1.fits.gz \
    kernel=ascii clob+
```
16. Extract the GTI extension of the events file.
```
fextract 6102/primary/6102_evt2.fits"[gti]" 6102/primary/raw.gti
```
17. Create the aspect histogram (CIAO format) from the aspect solution file.
```
asphist 6102/primary/pcadf211482871N003_asol1.fits.gz \
    6102/primary/6102.asphist \
    6102/primary/6102_evt2.fits
```
18. Create a soft band counts image (to look at) and crop it using the FOV
    region file.
```
dmcopy 6102/primary/6102_evt2.fits"[energy>500&&energy<4000&&sky=region(6102/primary/fov.reg)][bin sky=1]" 6102/0540.img
```
19. Open the image in DS9 and load the region file, then save the regions in
    WCS coordinates.
```
ds9 6102/0540.img -regions load 6102/primary/fov.reg \
    -regions format ds9 -regions system wcs -regions sky fk5 \
    -regions skyformat sexagesimal -regions save 6102/primary/fk5.reg
```


After the processing steps above, this is the current state of the 6102 folder:

```
└─ $ ll 6102/*
-rw-r--r--  1 qw    17M 6102/0540.img

6102/primary:
total 76064
drwxr-xr-x  11 qw   352B ./
drwxr-xr-x   5 qw   160B ../
-rw-r--r--   1 qw    65K 6102.asphist
-rw-r--r--   1 qw   6.1M 6102_evt2.fits
-rw-r--r--   1 qw   5.3M 6102_readoutbg.fits
-rw-r--r--   1 qw    22K acisf06102_000N003_bpix1.fits.gz
-rw-r--r--   1 qw   1.7K fk5.reg
-rw-r--r--   1 qw   1.1K fov.reg
-rw-r--r--   1 qw   2.6M pcadf211482871N003_asol1.fits.gz
-rw-r--r--   1 qw    23M pre_readout.fits
-rw-r--r--   1 qw   8.4K raw.gti

6102/secondary:
total 282720
drwxr-xr-x  20 qw   640B ./
drwxr-xr-x   5 qw   160B ../
-rw-r--r--   1 qw    96K 6102_bpix1.fits
-rw-r--r--   1 qw    52M 6102_evt1.fits
-rw-r--r--   1 qw   7.4K 6102_evt1.par
-rw-r--r--   1 qw    93K abb1_bpix1.fits
-rw-r--r--   1 qw    29M acisf06102_000N003_evt1.fits.gz
-rw-r--r--   1 qw   5.1K acisf06102_000N003_msk1.fits.gz
-rw-r--r--   1 qw   459K acisf06102_000N003_mtl1.fits.gz
-rw-r--r--   1 qw   327K acisf06102_000N003_stat1.fits.gz
-rw-r--r--   1 qw   437K acisf211481317N003_1_bias0.fits.gz
-rw-r--r--   1 qw   432K acisf211481317N003_2_bias0.fits.gz
-rw-r--r--   1 qw   436K acisf211481317N003_3_bias0.fits.gz
-rw-r--r--   1 qw   430K acisf211481317N003_4_bias0.fits.gz
-rw-r--r--   1 qw   437K acisf211481317N003_5_bias0.fits.gz
-rw-r--r--   1 qw   3.9K acisf211482490N003_pbk0.fits.gz
-rw-r--r--   1 qw    98K aglow_bpix1.fits
-rw-r--r--   1 qw   499B badpix_framestore_01236.txt
-rw-r--r--   1 qw   175B bias.lis
```


The next step is to examine the light curve of the Level=2 events to check for
flares, and filter them if there is any.

Before proceeding, we need to create a region file containing masks for point
sources. We also need a second region file that masks extended emission (this
is for filtering the light curve for flares, we want to use a relatively
source-free region so that we are looking at the fluctuations of the
background level).

For the point sources it will be easiest to see them if we combine the counts
images from all obsids. So let's process ObsID 1669 to Level=2 next. 6102 has
10 ks while 1669 has 43 ks, so 1669 helps a lot in increasing the signal to
noise of the image.

You can check the effective exposure by summing the GTI intervals. This can be
done by using gtiexp on a GTI extension. For example, an evt2 file has these
data blocks:

```
└─ $ dmlist 6102/primary/6102_evt2.fits blocks

--------------------------------------------------------------------------------
Dataset: 6102/primary/6102_evt2.fits
--------------------------------------------------------------------------------

     Block Name                          Type         Dimensions
--------------------------------------------------------------------------------
Block    1: PRIMARY                        Null
Block    2: EVENTS                         Table        15 cols x 98596    rows
Block    3: GTI3                           Table         2 cols x 1        rows
Block    4: GTI2                           Table         2 cols x 1        rows
Block    5: GTI1                           Table         2 cols x 1        rows
Block    6: GTI0                           Table         2 cols x 1        rows
Block    7: GTI6                           Table         2 cols x 2        rows

└─ $ gtiexp 6102/primary/6102_evt2.fits+2
   10506.6543
```

This looks at the 3rd data block (first one is `+0`, second `+1`, etc.) Each
chip has its own GTI extension but in general the total time is almost the
same.

And for 1669,

```
└─ $ gtiexp 1669/secondary/acisf01669_000N003_evt1.fits.gz+2
   42943.2344
```


---


### 2.2 Process ObsID 1669


This observation was taken in FAINT mode using a different set of CCDs.

```
└─ $ dmkeypar 1669/secondary/acisf01669_000N003_evt1.fits.gz DATAMODE echo+
FAINT

└─ $ dmkeypar 1669/secondary/acisf01669_000N003_evt1.fits.gz DETNAM echo+
ACIS-235678
```

The CCDs 5 and 7 are the two back-illuminated (BI) chips.

Directory listing before we start:

```
└─ $ ll 1669/*
1669/primary:
total 22216
drwxr-xr-x  4 qw   128B ./
drwxr-xr-x  4 qw   128B ../
-rw-r--r--  1 qw    30K acisf01669_000N003_bpix1.fits.gz
-rw-r--r--  1 qw    11M pcadf096953921N003_asol1.fits.gz

1669/secondary:
total 249560
drwxr-xr-x  14 qw   448B ./
drwxr-xr-x   4 qw   128B ../
-rw-r--r--   1 qw   104M acisf01669_000N003_evt1.fits.gz
-rw-r--r--   1 qw   5.4K acisf01669_000N003_msk1.fits.gz
-rw-r--r--   1 qw   1.8M acisf01669_000N003_mtl1.fits.gz
-rw-r--r--   1 qw   1.5M acisf01669_000N003_stat1.fits.gz
-rw-r--r--   1 qw   425K acisf096953118N003_0_bias0.fits.gz
-rw-r--r--   1 qw   493K acisf096953118N003_1_bias0.fits.gz
-rw-r--r--   1 qw   447K acisf096953118N003_2_bias0.fits.gz
-rw-r--r--   1 qw   425K acisf096953118N003_3_bias0.fits.gz
-rw-r--r--   1 qw   425K acisf096953118N003_4_bias0.fits.gz
-rw-r--r--   1 qw   442K acisf096953118N003_5_bias0.fits.gz
-rw-r--r--   1 qw   3.9K acisf096954419N003_pbk0.fits.gz
-rw-r--r--   1 qw   571B badpix_framestore_235678.txt
```


For this one the CCDs used are `235678`.

```
└─ $ dmkeypar 1669/secondary/acisf01669_000N003_evt1.fits.gz DETNAM echo+
ACIS-235678
```

Content of `1669/secondary/badpix_framestore_235678.txt`:

```
#ccd_id chipx_lo chipx_hi chipy_lo chipy_hi time time_stop bit action
#------ -------- -------- -------- -------- ---- --------- --- ------
2       2       1023    2       9       0       0       1       include
3       2       1023    2       9       0       0       1       include
5       2       1023    2       9       0       0       1       include
6       2       1023    2       9       0       0       1       include
7       2       1023    2       9       0       0       1       include
8       2       1023    2       9       0       0       1       include
```


---


#### Begin processing Level=1 to Level=2

The process below is similar to the one for 6102. For most of these commands,
only the file names have been updated with the appropriate values, so only the
ones that differ are commented on.

```
acis_clear_status_bits 1669/secondary/acisf01669_000N003_evt1.fits.gz

destreak infile=1669/secondary/acisf01669_000N003_evt1.fits.gz \
    outfile=1669/secondary/evt1_destreak.fits mask=None filter=no

dmmakepar 1669/secondary/evt1_destreak.fits 1669/secondary/1669_evt1.par clob+

cd 1669/secondary

ls -1 acisf*_?_bias*.fits* > bias.lis

cd ../..

acis_build_badpix obsfile=1669/secondary/1669_evt1.par \
    pbkfile=1669/secondary/acisf096954419N003_pbk0.fits.gz \
    biasfile=@1669/secondary/bias.lis \
    outfile=\!1669/secondary/abb1_bpix1.fits \
    usrfile=1669/secondary/badpix_framestore_235678.txt \
    bitflag=00000000000000011111100010012111 \
    calibfile=CALDB mode=h verbose=0

acis_find_afterglow infile=1669/secondary/evt1_destreak.fits \
    outfile=\!1669/secondary/aglow_bpix1.fits \
    badpixfile=1669/secondary/abb1_bpix1.fits \
    maskfile=1669/secondary/acisf01669_000N003_msk1.fits.gz \
    statfile=1669/secondary/acisf01669_000N003_stat1.fits.gz \
    mode=h verbose=0

acis_build_badpix obsfile=1669/secondary/1669_evt1.par \
    pbkfile=1669/secondary/acisf096954419N003_pbk0.fits.gz \
    biasfile=None \
    outfile=\!1669/secondary/1669_bpix1.fits \
    bitflag=00000000000000011111100010012111 \
    calibfile=1669/secondary/aglow_bpix1.fits \
    mode=h verbose=0

dmhedit 1669/secondary/evt1_destreak.fits none add BPIXFILE 1669_bpix1.fits

dmmakepar 1669/secondary/evt1_destreak.fits 1669/secondary/1669_evt1.par clob+
```

> **Note**: The `check_vf_pha` flag in `acis_process_events` is set to 'no'
> for 1669 because this was a FAINT mode observation.

```
acis_process_events infile=1669/secondary/evt1_destreak.fits \
    outfile=\!1669/secondary/1669_evt1.fits \
    badpixfile=1669/secondary/1669_bpix1.fits \
    acaofffile=1669/primary/pcadf096953921N003_asol1.fits.gz \
    mtlfile=1669/secondary/acisf01669_000N003_mtl1.fits.gz \
    eventdef=")stdlev1" \
    check_vf_pha=no

make_readout_bg 1669/secondary/evt1_destreak.fits \
    1669/primary/pre_readout.fits \
    1669/primary/pcadf096953921N003_asol1.fits.gz \
    1669/secondary/1669_bpix1.fits 0 CALDB CALDB \
    1669/secondary/acisf01669_000N003_mtl1.fits.gz

r4_header_update 1669/secondary/1669_evt1.fits

r4_header_update 1669/primary/pre_readout.fits

dmcopy 1669/secondary/1669_evt1.fits"[EVENTS][grade=0,2,3,4,6,status=b00000000000000000000000000x00000]" \!1669/primary/1669_evt2.fits

dmcopy 1669/primary/pre_readout.fits"[EVENTS][grade=0,2,3,4,6,status=b00000000000000000000000000x00000]" \!1669/primary/1669_readoutbg.fits

dmcopy 1669/primary/1669_evt2.fits"[cols -phas]" \!1669/primary/1669_evt2.fits

dmcopy 1669/primary/1669_readoutbg.fits"[cols -phas]" \!1669/primary/1669_readoutbg.fits

skyfov 1669/primary/1669_evt2.fits 1669/primary/fov.reg \
    aspect=1669/primary/pcadf096953921N003_asol1.fits.gz \
    mskfile=1669/secondary/acisf01669_000N003_msk1.fits.gz \
    kernel=ascii clob+

fextract 1669/primary/1669_evt2.fits"[gti]" 1669/primary/raw.gti

asphist 1669/primary/pcadf096953921N003_asol1.fits.gz \
    1669/primary/1669.asphist 1669/primary/1669_evt2.fits

dmcopy 1669/primary/1669_evt2.fits"[energy>500&&energy<4000&&sky=region(1669/primary/fov.reg)][bin sky=1]" \!1669/0540.img

ds9 1669/0540.img -regions load 1669/primary/fov.reg \
    -regions format ds9 -regions system wcs -regions sky fk5 \
    -regions skyformat sexagesimal -regions save 1669/primary/fk5.reg
```



Directory listing after processing these files:

```
└─ $ ll 1669/*
-rw-r--r--  1 qw    27M 1669/0540.img

1669/primary:
total 433736
drwxr-xr-x  11 qw   352B ./
drwxr-xr-x   5 qw   160B ../
-rw-r--r--   1 qw    73K 1669.asphist
-rw-r--r--   1 qw    50M 1669_evt2.fits
-rw-r--r--   1 qw    39M 1669_readoutbg.fits
-rw-r--r--   1 qw    30K acisf01669_000N003_bpix1.fits.gz
-rw-r--r--   1 qw   2.0K fk5.reg
-rw-r--r--   1 qw   1.3K fov.reg
-rw-r--r--   1 qw    11M pcadf096953921N003_asol1.fits.gz
-rw-r--r--   1 qw   106M pre_readout.fits
-rw-r--r--   1 qw   8.4K raw.gti

1669/secondary:
total 972128
drwxr-xr-x  21 qw   672B ./
drwxr-xr-x   5 qw   160B ../
-rw-r--r--   1 qw   121K 1669_bpix1.fits
-rw-r--r--   1 qw   174M 1669_evt1.fits
-rw-r--r--   1 qw   7.6K 1669_evt1.par
-rw-r--r--   1 qw   110K abb1_bpix1.fits
-rw-r--r--   1 qw   104M acisf01669_000N003_evt1.fits.gz
-rw-r--r--   1 qw   5.4K acisf01669_000N003_msk1.fits.gz
-rw-r--r--   1 qw   1.8M acisf01669_000N003_mtl1.fits.gz
-rw-r--r--   1 qw   1.5M acisf01669_000N003_stat1.fits.gz
-rw-r--r--   1 qw   425K acisf096953118N003_0_bias0.fits.gz
-rw-r--r--   1 qw   493K acisf096953118N003_1_bias0.fits.gz
-rw-r--r--   1 qw   447K acisf096953118N003_2_bias0.fits.gz
-rw-r--r--   1 qw   425K acisf096953118N003_3_bias0.fits.gz
-rw-r--r--   1 qw   425K acisf096953118N003_4_bias0.fits.gz
-rw-r--r--   1 qw   442K acisf096953118N003_5_bias0.fits.gz
-rw-r--r--   1 qw   3.9K acisf096954419N003_pbk0.fits.gz
-rw-r--r--   1 qw   127K aglow_bpix1.fits
-rw-r--r--   1 qw   571B badpix_framestore_235678.txt
-rw-r--r--   1 qw   210B bias.lis
-rw-r--r--   1 qw   174M evt1_destreak.fits
```


## 3. Clean for background flares

### 3.1 Select point sources and create a mask for `lc_clean`


Co-add the quick-look 0.5-4 keV counts images of the two ObsIDs. Check what
the wildcard filter selects. If it picks out the correct files, you can use
the wildcard and your shell will expand it into the list of files. Otherwise
you must type each of them manually.

```
└─ $ ll */0540.img
-rw-r--r--  1 qw    27M 1669/0540.img
-rw-r--r--  1 qw    17M 6102/0540.img

└─ $ addimages */0540.img 0540_coadd.img
Adding images :1669/0540.img 6102/0540.img
```

Since `*/0540.img` expands to `1669/0540.img 6102/0540.img` by the shell, it is
the same as the command

```
addimages 1669/0540.img 6102/0540.img 0540_coadd.img
```

Open this image in DS9:

```
ds9 0540_coadd.img
```

Set Scale > Square Root. Right click, hold and drag to adjust the bias and
contrast of the image. Edit > Region to use left click to draw regions. For
circular regions, single left click results in a circle with the default size.
Left click, hold, and drag lets you size the circle when it is created.

To save a region file, go to Region > Save Regions to open a "Save As" dialog
window. You can change the name of the file by editing the file path in the
Selection: text field. Accept the default `*.reg` format. Click OK. In the
next dialog window, Format should be `ds9` and Coordinate System should be
`fk5`.

Download my `pts.reg` and `lcclean_template.reg` that are attached as
examples. Put them in the folder `A478/reg/`. To open them, first open the
`0540_coadd.img`, then Region > Load Regions and in the dialog window that
shows, browse to the region file.

You can also do this from the command line:

```
ds9 0540_coadd.img -regions load reg/pts.reg \
    -regions load reg/lcclean_template.reg
```

This will open DS9, load the image, and open the two region files.

This page has a list of command line options for DS9.

http://ds9.si.edu/doc/ref/command.html

With these two region files we can proceed to the next step, checking for
background flares.


---


### 3.2 ObsID 6102 - Check for background flares


Go into the `6102/primary` directory, and create a region file named
`lcclean.reg`. It has the regions from three region files in this order:
`fk5.reg`, `lcclean_template.reg`, `pts.reg`.

The regions in `fk5.reg` are positive regions, while the regions copied from
the other two should be negative, by adding `-` before `circle(` (meaning they
are exclusion masks). See my `lcclean.reg` file. This `lcclean.reg` is the
mask that will be used to extract the light curve from to check for background
flares.

Take the template `lc_clean.par` file and edit the name of the events file to
match the Level=2 events file for this ObsID. You can use my `lc_clean.par`
file for this example.

Now run `lc_clean` in the `6102/primary` folder.

```
└─ $ lc_clean @lc_clean.par

This is lc_clean v140807

binsize= 1037.12
clip=3.00
max_factor= 1.200
6102_evt2.fits[events][regfilter('lcclean.reg')&&energy>2300&&energy<7300&&ccd_id!=5&&ccd_id!=7]   6041 evt

Calculated mean rate   0.5428 cts/s
Removing bins with rate outside   0.4524 -  0.6514 cts/s
Discarded   3246 s of bad time bins
(of them     0 s zero rate, approx  1037 s boundary bins and narrow GTIs)

Good time left    7259 s
Mean cleaned rate   0.5367 cts/s
Wrote clean.gti
```

This generates 3 files. `rawacis.lc` and `clean.lc` are text files of count
rates in binned time intervals before and after filtering. `clean.gti` is the
GTI file after filtering for flares. We will use `clean.gti` to filter
`evt2.fits` to get the final product.

The script `plot_lc` in `acisgenie/bin` will create a figure using
`rawacis.lc` and `clean.lc` that you can view to see the the light curve and
the effect of the filter.

```
plot_lc clean.lc rawacis.lc
```

(Creates file `clean.lc.eps`, which you can open with `gv` from ghostscript
package or convert to PDF using `eps2pdf`.)

Always check this to make sure this step has been done correctly. `lc_clean`
applies a cut at +/-20% from the mean count rate, and is appropriate only for
short flares that show as spikes in the light curve. It does not work for long
flares that involves slowly rising or slowly falling count rate. If you
encounter the latter, you must manually specify the beginning and end
timestamps in `lc_clean.par`.

The authoritative description for the light curve cleaning procedure is in the
`COOKBOOK` file in your `chav/acisbg` source. At this time it is also
available online at http://cxc.harvard.edu/contrib/maxim/acisbg/COOKBOOK. The
CIAO thread explaining this is at http://cxc.harvard.edu/ciao/threads/flare/.

As it turns out, 6102 suffers from a flare towards the end of its exposure.
You will see two grey time bins in the figure, accounting for the 3 ks that
was discarded by `lc_clean`. The remaining time bins range between 0.5-0.55
counts/s, which is OK. So for this observation, this automatic filter is
sufficient and we can proceed with using the resulting `clean.gti` file.

Go back up to the folder `A478/6103`. Make the directory `6103/processed` that
will hold the cleaned event files. Do the following:


1. Filter evt2 file using cleaned GTI and name it evt3.fits
```
dmcopy primary/6102_evt2.fits'[@primary/clean.gti][ccd_id!=5&&ccd_id!=7]' \!processed/evt3.fits
```
2. Also filter the readout background events
```
dmcopy primary/6102_readoutbg.fits'[@primary/clean.gti][ccd_id!=5&&ccd_id!=7]' \!processed/readoutbg.fits
```
3. And the aspect solution file
```
dmcopy primary/pcadf211482871N003_asol1.fits.gz'[@primary/clean.gti]' \!processed/asol1.fits
```
4. Create CIAO aspect histogram file from filtered aspect solution
```
asphist processed/asol1.fits \!processed/asphist.fits processed/evt3.fits
```
5. A.V.'s aoff1 and aspecthist (an image) files needed for later analysis
```
asol2aoff -o \!processed/aoff1.fits -evtfile processed/evt3.fits -asol1 processed/asol1.fits
aspecthist aspfile=processed/aoff1.fits -o \!processed/aspecthist.fits -gtifile primary/clean.gti
```
6. Make a symlink to clean.gti
```
ln -s ../primary/clean.gti processed/clean.gti
```
7. Make soft counts file to be used in later spectral analysis
```
dmcopy processed/evt3.fits'[energy>500&&energy<2000]' \!jnk052
dmcopy jnk052'[exclude sky=region(../reg/pts.reg)]' \!processed/052_noso.fits
rm -f jnk052
```


---


### 3.3 ObsID 1669 - Check for background flares


We repeat the light curve cleaning procedure, but do it separately for the FI
and BI chips.

Go into the directory 1669/primary, and create lcclean.reg in a similar
manner, but using regions from fk5.reg of 1669. Remember to replace 'circle'
with '-circle' for the regions meant to be excluded.

We can use the same lcclean.reg file for both FI and BI chips. The only caveat
is if there is only one BI chip, and then region masking the extended cluster
emission covers most or all of it. Then there will be no regions to extract a
light curve from. In that case, you should make a copy of lcclean.reg for
filtering the BI chip (naming it lcclean_BI.reg to be clear). Reduce the mask
in that one to have some usable region where the emission is dimmest.

For 1669 both chips 5 and 7 were in operation so we don't need to worry about
that.

Look at the FI chips first. Take my `lc_clean.par`, and note what is different
between it and `lc_clean_BI.par` (the `ccd_id` filter) in the `file1`
argument. Run `lc_clean`:

```
└─ $ lc_clean @lc_clean.par

This is lc_clean v140807

binsize= 1037.12
clip=3.00
max_factor= 1.200
1669_evt2.fits[events][regfilter('lcclean.reg')&&energy>2300&&energy<7300&&ccd_id!=5&&ccd_id!=7]  17858 evt

Calculated mean rate   0.4144 cts/s
Removing bins with rate outside   0.3453 -  0.4973 cts/s
Discarded    421 s of bad time bins
(of them     0 s zero rate, approx  1037 s boundary bins and narrow GTIs)

Good time left   42521 s
Mean cleaned rate   0.4159 cts/s
Wrote clean.gti
```

Plot the results:

```
plot_lc clean.lc rawacis.lc
```

So the count rate ranges between ~0.38-0.48 counts/s, and does not exceed
+/-20% from the mean. It does not look like there is a strong background flare
in the light curve.

Do the same for the BI chips:

```
└─ $ lc_clean @lc_clean_BI.par

This is lc_clean v140807

binsize= 1037.12
clip=3.00
max_factor= 1.200
1669_evt2.fits[events][regfilter('lcclean.reg')&&energy>2300&&energy<7300&&ccd_id>4&&ccd_id<8&&ccd_id!=6]   9839 evt

Calculated mean rate   0.2275 cts/s
Removing bins with rate outside   0.1896 -  0.2730 cts/s
Discarded   1458 s of bad time bins
(of them     0 s zero rate, approx  1037 s boundary bins and narrow GTIs)

Good time left   41484 s
Mean cleaned rate   0.2275 cts/s
Wrote clean_BI.gti
```

Plot the results.

```
plot_lc clean_BI.lc rawacis_BI.lc
```

The light curve cleaning filter has identified one time bin outside of the
acceptable range.

Go back up to the folder `A478/1669`. Make the directory `1669/processed` that
will hold the cleaned event files. The procedure is the same as for 6102,
except we repeat it for BI chips (with appropriate changes to the commands for
the `ccd_id` filter and file names).


```
# FI chips
dmcopy primary/1669_evt2.fits'[@primary/clean.gti][ccd_id!=5&&ccd_id!=7]' \!processed/evt3.fits
dmcopy primary/1669_readoutbg.fits'[@primary/clean.gti][ccd_id!=5&&ccd_id!=7]' \!processed/readoutbg.fits
dmcopy primary/pcadf096953921N003_asol1.fits.gz'[@primary/clean.gti]' \!processed/asol1.fits
# CIAO aspect histogram file
asphist processed/asol1.fits \!processed/asphist.fits processed/evt3.fits
# A.V.'s aoff1 and aspecthist (an image) files
asol2aoff -o \!processed/aoff1.fits -evtfile processed/evt3.fits -asol1 processed/asol1.fits
aspecthist aspfile=processed/aoff1.fits -o \!processed/aspecthist.fits -gtifile primary/clean.gti

ln -s ../primary/clean.gti processed/clean.gti

# Make soft counts file
dmcopy processed/evt3.fits'[energy>500&&energy<2000]' \!jnk052
dmcopy jnk052'[exclude sky=region(../reg/pts.reg)]' \!processed/052_noso.fits
rm -f jnk052


# BI chips
dmcopy primary/1669_evt2.fits'[@primary/clean_BI.gti][ccd_id=5,7]' \!processed/evt3_BI.fits
dmcopy primary/1669_readoutbg.fits'[@primary/clean_BI.gti][ccd_id=5,7]' \!processed/readoutbg_BI.fits
dmcopy primary/pcadf096953921N003_asol1.fits.gz'[@primary/clean_BI.gti]' \!processed/asol1_BI.fits
# CIAO aspect histogram file
asphist processed/asol1_BI.fits \!processed/asphist_BI.fits processed/evt3_BI.fits
# A.V.'s aoff1 and aspecthist (an image) files
asol2aoff -o \!processed/aoff1_BI.fits -evtfile processed/evt3_BI.fits -asol1 processed/asol1_BI.fits
aspecthist aspfile=processed/aoff1_BI.fits -o \!processed/aspecthist_BI.fits -gtifile primary/clean_BI.gti

ln -s ../primary/clean_BI.gti processed/clean_BI.gti

# Make soft counts file
dmcopy processed/evt3_BI.fits'[energy>500&&energy<2000]' \!jnk052
dmcopy jnk052'[exclude sky=region(../reg/pts.reg)]' \!processed/052_noso_BI.fits
rm -f jnk052
```


---
