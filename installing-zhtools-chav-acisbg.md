# Installing ZHTOOLS, CHAV, and ACISBG

_2019-05-18_

## Part 1. Install prerequisites: Funtools and WCS

> Follow the steps below to obtain the latest version of Funtools and install
> into `$HOME/astro/soft/funtools`.
>
> When compiling against Funtools, the lib and include folders to specify will
> be:
>
```
$HOME/astro/soft/funtools/lib
$HOME/astro/soft/funtools/include
```

---

### Funtools

Start a fresh terminal session.

```
cd $HOME/astro/soft

git clone https://github.com/ericmandel/funtools.git

cd funtools

./mkconfigure

./configure --prefix=$HOME/astro/soft/funtools

make
```

The make install script is missing the `-p` flag and will fail to copy
manpages, so make this folder beforehand, then install.

```
mkdir -p $HOME/astro/soft/funtools/share/man

make install
```

For some reason the header files are not in `include/` but
`include/funtools/`. Need to move them up one level or some config scripts
won't find them.

```
mv include/funtools/* include/
```

### WCS

Finally, make a symlink in `lib/` that will use `libfuntools.a` as `libwcs.a`.
The latter has been built as a subpackage so all of its symbols will be in the
library. The `include/` folder has `wcs.h` and `wcslib.h`, so the `funtools/`
installation can double as wcstools for zhtools.

```
ln -s libfuntools.a lib/libwcs.a
```


---


## Part 2. ZHTOOLS, CHAV, and ACISBG

> Follow the instructions here and install `zhtools`, `chav`, and `acisbg`, in
> this order.


### ZHTOOLS


This installs Alexey Vikhlinin's `zhtools`, which has numerous useful tools
for manipulating FITS images and for X-ray data analysis. It appears to be no
longer being developed and unavailable from the original URL. A copy is
available in the repository below. This version includes a modification to the
floodfill algorithm in `wvdecomp` to enable it to process large images.

```
cd $HOME/astro/soft

git clone https://gitlab.com/qwq/astro-zhtools.git

mv astro-zhtools zhtools; cd zhtools
```

We will use the `readline` and `cfitsio` libraries that come with CIAO to
avoid any compatibility issues.

```
# Copy the command below exactly, there must not be any space after '\'
./configure --prefix=$HOME/astro/soft/zhtools \
    --with-readline=$HOME/astro/soft/ciao-4.11/ots \
    --with-funtools=$HOME/astro/soft/funtools \
    --with-cfitsio=$HOME/astro/soft/ciao-4.11/ots \
    --with-wcs=$HOME/astro/soft/funtools \
    FFLAGS=-fbackslash

make

make install
```

Add `zhtools/bin/` to `$PATH` in your `~/.bashrc`:

Find the last line that says `export PATH=...` and _prepend_ the value with
`$HOME/astro/soft/zhtools/bin`. If there is no such line yet, add one (you can
also add this line even if you have one already, it just prepends the `$PATH`
with `zhtools/bin`):

```
export PATH=$HOME/astro/soft/zhtools/bin:$PATH
```

If you use another shell, also update the `$PATH` in its rc file.


---


### CHAV


This sets up Alexey Vikhlinin's CHAV data reduction tools. They are still
available online; this repository includes some minor modifications from me.

```
cd $HOME/astro/soft

git clone https://gitlab.com/qwq/astro-chandra-chav.git

mv astro-chandra-chav chav; cd chav
```

Decompress calibration files in the `CAL/` folder:

```
find CAL -iname "*.gz" -exec gzip -d {} \;
```

We will again use the `readline` and `cfitsio` libraries that come with CIAO
to avoid any compatibility issues.

```
cd chav
```

Copy the command below exactly, there must not be any space after `\`

```
./configure --prefix=$HOME/astro/soft/chav \
    --with-ciao=$HOME/astro/soft/ciao-4.11 \
    --with-cfitsio=$HOME/astro/soft/ciao-4.11/ots \
    --with-zhtools=$HOME/astro/soft/zhtools \
    --with-readline=$HOME/astro/soft/ciao-4.11/ots \
    --with-mekal=$HOME/astro/soft/heasoft-6.25/spectral/modelData/mekal.mod \
    FFLAGS=-fbackslash

make

make install
```

Now also add `$HOME/astro/soft/chav/bin` to your `$PATH`.


### ACISBG


This sets up Maxim Markevitch's ACIS background tool. They are still available
online.

```
cd $HOME/astro/soft/chav/acisbg
```

We will install this also into the `chav/` folder

```
./configure --prefix=$HOME/astro/soft/chav \
    --with-ciao=$HOME/astro/soft/ciao-4.11 \
    --with-cfitsio=$HOME/astro/soft/ciao-4.11/ots \
    FFLAGS=-fbackslash

make

make install
```


---


## Part 3. Check that things work


Now start a new shell session. Try running the following:

```
# zhtools program
imexam

# chav program
fitscopy

# acisbg program
make_acisbg
```

You should get an informational message from each of them.


---
