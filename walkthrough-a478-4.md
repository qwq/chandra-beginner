# Data analysis walkthrough for Abell 478 (Part 4)

_2019-06-24_

We will fit the spectrum with a single-temperature model in Xspec. Doing this
for a spectrum extracted from a large aperture on the cluster provides us the
needed parameters for making the exposure map for imaging analysis.


This next part involves spectral analysis in XSPEC. Start `xspec` and `cd`
into `spec/c3`.

Group the c3.pi spectrum into minimum 100 count bins:

```tcl
grppha c3.pi c3_100.pi
```

In the prompt that shows up for GRPPHA, first type

```
GRPPHA] group min 100
```

Then type

```
GRPPHA] exit
```

A new file named `c3_100.pi` has been created in which the spectra data is
binned to minimum of 100 counts per bin.

Load the spectrum file:

```tcl
data c3_100.pi
```

Open a plotting window:

```tcl
cpd /xw
```

Make it plot energy:

```tcl
setp en
```

Plot it:

```tcl
plot
```

This doesn't show very much. You can add a power law model with zero
normalization to check whether the ACIS background subtraction works well.

```tcl
model pow
0
0

setp background
ignore **; notice .5-12.
plot data delchi
```

Zoom in to the hard energies:

```tcl
ignore **-7.
pl
```

Now you should see better that the background dominates at these energies.
Between 9 and 12 keV, the background subtracted flux is basically zero, and in
the delta chi plot the data points should be consistent with 0. If the
residuals are systematically going above or below zero with increasing energy,
then the background subtraction has a problem.


Now fit a single-temperature APEC model with simple absorption.

```tcl
model apec*tbabs
8
.3
0.0881
1e-4
.1

notice **; ignore **-.8 9.-**
renorm
fit 10000 1e-4
pl
```

You'll see this info print after running fit above:

```
========================================================================
Model apec<1>*TBabs<2> Source No.: 1   Active/On
Model Model Component  Parameter  Unit     Value
par  comp
   1    1   apec       kT         keV      6.34592      +/-  6.48459E-02  
   2    1   apec       Abundanc            0.300000     frozen
   3    1   apec       Redshift            8.81000E-02  frozen
   4    1   apec       norm                6.76291E-02  +/-  2.65169E-04  
   5    2   TBabs      nH         10^22    0.309790     +/-  2.81789E-03  
________________________________________________________________________


Fit statistic : Chi-Squared =         483.39 using 434 PHA bins.

Test statistic : Chi-Squared =         483.39 using 434 PHA bins.
Reduced chi-squared =         1.1215 for    431 degrees of freedom 
Null hypothesis probability =   4.099575e-02
```

When we set up the model, I did not know any of the parameters except the
redshift, which I looked up. So 8 keV, 0.3 solar abundance, and 0.1 for nH are
just reasonable guesses. After `renorm` and `fit`, the unfrozen parameters are
tuned to close to their best fit values anyway.

After running this fit with redshift and abund frozen, run it again with
abundance free.

```tcl
thaw 2
fit
pl
```

```
========================================================================
Model apec<1>*TBabs<2> Source No.: 1   Active/On
Model Model Component  Parameter  Unit     Value
par  comp
   1    1   apec       kT         keV      6.34626      +/-  6.36997E-02  
   2    1   apec       Abundanc            0.347918     +/-  1.11119E-02  
   3    1   apec       Redshift            8.81000E-02  frozen
   4    1   apec       norm                6.69051E-02  +/-  3.11565E-04  
   5    2   TBabs      nH         10^22    0.309739     +/-  2.81763E-03  
________________________________________________________________________


Fit statistic : Chi-Squared =         464.34 using 434 PHA bins.

Test statistic : Chi-Squared =         464.34 using 434 PHA bins.
Reduced chi-squared =         1.0799 for    430 degrees of freedom 
Null hypothesis probability =   1.222665e-01
```

The plot looks pretty good; the delta chi points (lower panel) are mostly
between +/-1 and there is no systematic deviations.

Around 6 keV are the iron line complexes. There is very good signal here for
A478. One thing this feature can be used for is to fit the model redshift (see
the wavy thing in the residuals - there's a slight misfit).

```tcl
thaw 3
fit
pl
```

```
========================================================================
Model apec<1>*TBabs<2> Source No.: 1   Active/On
Model Model Component  Parameter  Unit     Value
par  comp
   1    1   apec       kT         keV      6.32553      +/-  6.36948E-02  
   2    1   apec       Abundanc            0.350525     +/-  1.11776E-02  
   3    1   apec       Redshift            8.59492E-02  +/-  6.67562E-04  
   4    1   apec       norm                6.67097E-02  +/-  3.16216E-04  
   5    2   TBabs      nH         10^22    0.310167     +/-  2.81524E-03  
________________________________________________________________________


Fit statistic : Chi-Squared =         447.08 using 434 PHA bins.

Test statistic : Chi-Squared =         447.08 using 434 PHA bins.
Reduced chi-squared =         1.0421 for    429 degrees of freedom 
Null hypothesis probability =   2.639197e-01
```

The fit at the iron line complex has improved. The best fit redshift is ~2.3%
lower than the lookup value. This doesn't mean that we have determined a new
redshift value for the cluster. Rather, the ACIS detector gain fluctuates with
time and this small adjustment compensates for any gain calibration error that
may be present in the data.

Now try to find the statistical errors on the parameters:

```tcl
err 1 2 3 4 5
```

```
Parameter   Confidence Range (2.706)
     1      6.22072      6.41609    (-0.104262,0.0911123)
     2     0.333774     0.370463    (-0.0192605,0.0174292)
Apparent non-monotonicity in statistic space detected.
Current bracket values 0.0851336, 0.084106
and delta stat 2.32026, 9.55732
but latest trial 0.0850542 gives 2.22664
Suggest that you check this result using the steppar command.
Apparent non-monotonicity in statistic space detected.
Current bracket values 0.0866808, 0.0870681
and delta stat 1.36008, 5.03339
but latest trial 0.0868528 gives 5.5488
Suggest that you check this result using the steppar command.
     3    0.0846198    0.0868745    (-0.00168246,0.000572206)
Apparent non-monotonicity in statistic space detected.
Current bracket values 0.0671221, 0.0672341
and delta stat 2.47655, 5.36448
but latest trial 0.0671338 gives 2.25929
Suggest that you check this result using the steppar command.
     4    0.0662238    0.0671781    (-0.000490102,0.000464172)
Apparent non-monotonicity in statistic space detected.
Current bracket values 0.310232, 0.305974
and delta stat 0, 2.77285
but latest trial 0.306034 gives 2.77359
Suggest that you check this result using the steppar command.
     5     0.308103     0.314897    (-0.0021292,0.00466479)
```

If you run fit again, the best fit values have changed:

```
========================================================================
Model apec<1>*TBabs<2> Source No.: 1   Active/On
Model Model Component  Parameter  Unit     Value
par  comp
   1    1   apec       kT         keV      6.32507      +/-  6.36941E-02  
   2    1   apec       Abundanc            0.352946     +/-  1.11686E-02  
   3    1   apec       Redshift            8.62400E-02  +/-  5.23713E-04  
   4    1   apec       norm                6.67090E-02  +/-  3.14418E-04  
   5    2   TBabs      nH         10^22    0.310238     +/-  2.81741E-03  
________________________________________________________________________


Fit statistic : Chi-Squared =         446.49 using 434 PHA bins.

Test statistic : Chi-Squared =         446.49 using 434 PHA bins.
Reduced chi-squared =         1.0408 for    429 degrees of freedom 
Null hypothesis probability =   2.703443e-01
```

This is OK. What happens is that the parameters 3 4 5 do not have a smooth
minimum in their chisquare curves. For example, if we steppar for redshift
near the best fit,

```tcl
steppar 3 0.085 0.087 50
pl con
```

That bumpiness makes the chisquare curves non-monotonic, so the fit may settle
on any one of the local minima near the bottom. The key is that the confidence
ranges are bounded and reasonably small, for all parameters, and the jitters
in the best fit values are within the confidence ranges (that green line is
the minimum chisquare +2.706, corresponding to the 90% confidence level
range).

We are done with the cluster average spectrum. Run fit again and save the
results to a file.

```tcl
fit
save all bestfit
```

This creates a file `bestfit.xcm` that you can load next time (`@bestfit.xcm`)
to have the files and model loaded like the current state.


Fill `A478.json` with the best fit parameters you obtained.


---
