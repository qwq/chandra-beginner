# Installing CIAO and HEASOFT on Ubuntu 16.04 LTS

_2019-05-15_

> This note follows setting up CIAO on a fresh install of Ubuntu 16.04 LTS from
> the MS app store. This Ubuntu runs as a Windows Subsystem Linux in Windows 10.
> Installing CIAO and HEASOFT on an actual Ubuntu system is easier because more
> packages are already present. Also, the precompiled CIAO installation for
> Linux will probably work, and then you will not need to compile CIAO and DS9
> from source.

> At time of writing, Ubuntu 18.04 is difficult, if not impossible, to work
> with. It ships with a newer, but incompatible, libssl library for CIAO.


[Preparations](#preparations)

[Install CIAO](#installing-ciao)

[Install CALDB & ACIS Background](#install-caldb-with-acis-blank-sky-background-data)

[Compile DS9 from source](#compiling-saoimage-ds9-from-source)

[Install HEASOFT from source](#installing-heasoft-from-source)

## Preparations

After creating the new user account in Ubuntu, start installing some software
package tools. Start with synaptic (package manager).

```
sudo apt-get update
sudo apt-get install synaptic
```

The program will automatically install the dependencies as well. Wait for it to
finish.

Edit `.bashrc` to forward X to Windows:

```
vim $HOME/.bashrc
```

Press `G` (case sensitive) to jump to the last line of the file, then `A` to
enter 'insert mode' at the end of the current line. Add a few newlines and paste
this in (in this Ubuntu-in-Windows terminal it's usually `Ctrl + Shift + V`, or
`mouse rightclick` to paste):

```
# Strip windows paths from PATH
export PATH=$(/usr/bin/printenv PATH | /usr/bin/perl -ne 'print join(":", grep { !/\/mnt\/[a-z]/ } split(/:/));')

alias rm='rm -i'   # Prompt for confirmation for rm
alias l=less       # Less typing

# Run screens from user directory
export SCREENDIR=$HOME/.screen

# Forward to X server in Windows
export DISPLAY=localhost:0.0
```

Press `Esc` to leave insert mode. Then type `:wq` to write to file and quit vim.
(Read more about the basic commands and how to custom Vim.)

> Start the X window server in Windows before trying to run an X11 program such
> as synaptic.

Now run Synaptic.

```
sudo synaptic
```

Use the Search function, and look for `build-essential` by name. Mark this
result for installation, and confirm when prompted about dependencies. Then
click Apply, confirm the dialog and wait for it to finish.

You should also install the following, since they are probably going to be
needed at some point.

```
gfortran
libncurses5-dev
libreadline6-dev
libglu1-mesa
libglu1-mesa-dev
```

> Whenever you get errors about missing lib or header files, google the name to
> see if they're part of some package, and find out if you can install them via
> apt-get or synaptic).

Synaptic has an easy to use GUI that lets you browse detailed info on each
package. If you know what you need and just want to get it done, you can also do

```
sudo apt-get install ________
```

For example, to install all of the packages in the above list,

```
sudo apt-get install gfortran libncurses5-dev libreadline6-dev libglu1-mesa libglu1-mesa-dev
```


---


## Installing CIAO

> At time of writing of this note, the current version was CIAO 4.11 and CALDB
> 4.8.2.

We start with a customized install script from here:

http://cxc.harvard.edu/ciao/download/custom.html

Uncheck 'Base CALDB v4.8.2'. The calibration files can be downloaded later,
separately.

> **Will you need to compile from source?** If you are installing on an actual
> Linux system, check first whether the precompiled CIAO installation works. If
> so, you can skip compiling CIAO and DS9 from source.

For Source build, select Full. Otherwise, select the operating system that best
describes your system.

Click the button to download the `ciao-install` script.

Copy the file into the Ubuntu filesystem. The Windows `C:\` drive is mounted at
`/mnt/c/`

```
mkdir -p $HOME/astro/soft/ciao-4.11-install
```

Place the `ciao-install` script there, and make it executable:

```
chmod +x ciao-install
```

Run it:

```
./ciao-install
```

**Configuration for ciao-install:**

- Download directory, choose $HOME/astro/soft/ciao-4.11-install
- Installation directory, choose $HOME/astro/soft
  (Doing this will put the software in $HOME/astro/soft/ciao-4.11)
- Run smoke test (yes)
- Delete tar files (no)
- Save settings (yes)

When this is done, it should pass all but 2 tests (ds9 and obsvis). To fix
those, you would need to compile your own DS9 from source (currently version
8.0) and then specify it in `$HOME/.ciaorc`. (This is in a different section.)

Per CIAO install instructions, add the following line to `$HOME/.bashrc` to
create an alias `ciao` that will initialize the shell environment.

```
# Initialize CIAO environment
alias ciao="source $HOME/astro/soft/ciao-4.11/bin/ciao.bash"
```

The next time a Bash shell starts, it will have this alias ready, and you can
initialize the CIAO environment by typing `ciao` on the command line.


---

## Install CALDB with ACIS Blank Sky Background Data

_2019-05-17_

CALDB is a particular organization standard for calibration files for high
energy astrophysics missions, The HEASARC Calibration Database. CIAO's CALDB
is modelled after it and uses the same layout. The standard CIAO installation
puts a copy of its CALDB in the folder `ciao-4.11/CALDB`, and the set up
script `ciao-4.11/bin/ciao.sh` looks for this folder to determine if CALDB is
installed.

Because CALDB updates less frequently than CIAO, it is not necessary to
download and unpack a full copy of it each time CIAO is updated. CALDB can be
located elsewhere, and a symlink to it placed in `ciao-4.11` will do. If you
have multiple CIAO versions installed, you only need the most up-to-date CALDB
to which every CIAO installation can point. If you have calibration files for
other X-ray missions, they can all be kept in the same CALDB folder.

Follow the steps below to obtain a copy of CALDB 4.8.2 and ACIS blank sky
background files, and make a link to the CALDB folder for CIAO.

```
mkdir $HOME/astro/soft/CALDB
cd $HOME/astro/soft/CALDB

wget ftp://cda.harvard.edu/pub/arcftp/caldb/caldb_4.8.2_main.tar.gz
```

> A tip with these tar files. Always look inside to see what the folder
> structure is like before extracting files! If you don't, you risk exploding
> files all over the current working directory. The `tar tf` option for tar
> lists files in the archive.

```
tar tf caldb_4.8.2_main.tar.gz | less

./software/
./software/tools/
./software/tools/alias_config.fits
./software/tools/caldb.config
./software/tools/caldbinit.sh
...
```

In this case there is no top level container for these files. If we downloaded
it to `astro/soft` and extracted there, it would be bad.

Since we downloaded this to `astro/soft/CALDB`, we can extract everything here.

```
tar xf caldb_4.8.2_main.tar.gz
```

CALDB 4.8.2 is now located at `$HOME/astro/soft/CALDB`.

```
wget ftp://cda.harvard.edu/pub/arcftp/caldb/acis_bkgrnd_4.7.6.tar.gz
```

This archive also does not have a top level container. We are extracting it
into the CALDB folder.

```
tar xf acis_bkgrnd_4.7.6.tar.gz
```

ACIS blank sky background files are now located in
`CALDB/data/chandra/acis/bkgrnd`.

Now go to the CIAO installation and add a symlink to the CALDB folder. If you
used the same directory layout as these instructions, it will be the following:

```
ln -s $HOME/astro/soft/CALDB $HOME/astro/soft/ciao-4.11/CALDB
```

Now when you start CIAO, it will find CALDB:

```
qw@DESKTOP-V1E2O0K:~$ ciao
CIAO configuration is complete...
CIAO 4.11 Wednesday, December  5, 2018
  bindir      : /home/qw/astro/soft/ciao-4.11/bin
  CALDB       : 4.8.2
```

---

## Compiling SAOImage DS9 from Source

_2019-05-16_

> This is only necessary if the precompiled version of DS9 that comes with CIAO
> does not work. I have only found need to do this on a Windows Subsystem Linux
> system. To check if CIAO's DS9 works, first initialize the CIAO environment by
> entering `ciao` on the command line, then try to launch DS9 by entering `ds9`.

>DS9 requires a running X server. If running on WSL, the `$DISPLAY` environment
>parameter must be set. Refer to the earlier part of the notes.

Install some packages:

```
sudo apt-get install zlib1g-dev libssl-dev libxml2-dev libxslt-dev autoconf tcl-dev tk-dev zip
```

(More may be needed if you had not compiled CIAO from source before starting
this.)


Download SAOImage DS9 version 8.0 source:

```
mkdir -p $HOME/astro/soft
cd $HOME/astro/soft

wget http://ds9.si.edu/archive/source/ds9.8.0.tar.gz

tar xvf ds9.8.0.tar.gz

mv SAOImageDS9 saods9-8.0
cd saods9-8.0

unix/configure >& config.log
```

When configure finishes running, check the log (`less config.log`) and scroll to
the bottom (press `G`) to see that it completed and did not error out.

Then build from source:

```
make >& build.log
```

Wait for it to finish, then check the log file `build.log` to see if it
completed. A successful compilation ends with the following message:

```
...
chmod 755 ds9
cp ds9 /home/qw/astro/soft/saods9-8.0/bin/.
make[1]: Leaving directory '/home/qw/astro/soft/saods9-8.0/ds9/unix'
```

Check that DS9 will launch (again, if you are running a Windows Subsystem Linux
you need to have X server running within Windows and `$DISPLAY` set in
`.bashrc`):

```
$HOME/astro/soft/saods9-8.0/bin/ds9
```

Now do the following two things:

1. Add ds9 to a directory that is in $PATH so that you can run it by typing
'ds9' anywhere, whether or not you are using the CIAO environment. In my case
$HOME/bin is in the path by default, but the folder doesn't exist yet. So I
created it and made a symbolic link to the ds9 executable.
```
mkdir $HOME/bin
ln -s $HOME/astro/soft/saods9-8.0/bin/ds9 $HOME/bin/ds9
```
(Read about symlinks. The command to create one is `ls -s [target path] [link
path]`. [link path] can be absolute or relative to your current directory;
[target path] can be absolute or relative to the [link path].)

2. Check that it works:
```
qw@DESKTOP-V1E2O0K:~$ which ds9
/home/qw/bin/ds9
```

3. Specify this DS9 executable to be used by CIAO instead of the OTS version.
This is done in `$HOME/.ciaorc`.
```
vim $HOME/.ciaorc
```
Find these lines, uncomment the line beginning with `#DS9` and replace it with
the FULL path to the bin directory. It did not work for me if `~` is used to
substitute for `$HOME`.
```
# Replace /usr/local/bin with path to ds9 to force use of local ds9
DS9 /home/qw/astro/soft/saods9-8.0/bin
```

4. Start a fresh shell session, and initialize CIAO. Then check the following
work:
```
ds9
obsvis
```
These were the two smoke tests that failed during the source build for CIAO. Now
all of its features should work.


---


## Installing HEASOFT from source

_2019-05-17_

> It is recommended that HEASOFT is installed from source regardless of what OS
> you use.

Install prerequisite packages (plus any others as needed to compile):

```
sudo apt-get install libreadline6-dev libcurl4-gnutls-dev libncurses5-dev xorg-dev gcc g++ gfortran perl python3-dev
```

Download and unpack HEASOFT-6.25 source:

```
mkdir -p $HOME/astro/soft

cd $HOME/astro/soft

wget http://heasarc.gsfc.nasa.gov/FTP/software/lheasoft/release/heasoft-6.25src.tar.gz

tar xvf heasoft-6.25src.tar.gz
```

Configure and build:

```
cd heasoft-6.25/BUILD_DIR

export PYTHON=$(which python3)

./configure >& config.log
```

(We are building this in-place, i.e. not installing the compiled binaries into a
different directory)

Check `config.log` to see that it finished running and did not halt at an error message. The
very last lines in the output should read:

```
## ----------- ##
## confdefs.h. ##
## ----------- ##

/* confdefs.h */
#define PACKAGE_NAME ""
#define PACKAGE_TARNAME ""
#define PACKAGE_VERSION ""
#define PACKAGE_STRING ""
#define PACKAGE_BUGREPORT ""
#define PACKAGE_URL ""

configure: exit 0
```

> **PyXspec**
>
> Now check for the version of Python that Xspec will be compiled against. This
> is for PyXspec: we want it to be compiled for Python 3 (the default picks
> Python 2 first).
```
grep "PYTHON_" $HOME/astro/soft/heasoft-6.25/Xspec/BUILD_DIR/hmakerc
```
Look for the lines beginning `PYTHON_INC=` and `PYTHON_LIB=`. They should be
pointing to a `python3.x` (`python3.5` if you followed all of the instructions so
far). If not, you must find where the Python3 include and lib directories are
and edit that file `hmakerc` to point to them.


Now run make:

```
make >& build.log
```

Wait for it to finish then check `build.log` to see if it stopped at an error
(warnings during compilation are OK, just ignore them). The last few lines of a
successful build say:

```
make[2]: Leaving directory '/home/qw/astro/soft/heasoft-6.25/heasim/BUILD_DIR'
make[1]: Leaving directory '/home/qw/astro/soft/heasoft-6.25/BUILD_DIR'
Finished make all
```

After that, do this final step to place the compiled files under
`heasoft-6.25/(PLATFORM)`.

```
make install
```

On the Ubuntu 16.04 LTS WSL for example, the platform name is
`x86_64-pc-linux-gnu-libc2.23`. Check what this is in your installation.

The last few lines when install is successful are:

```
make[1]: Leaving directory '/home/qw/astro/soft/heasoft-6.25/BUILD_DIR'
Finished make install
```

Setup HEASOFT environment:

Add the alias for sourcing the environment initialization script to `.bashrc`:

```
vim ~/.bashrc
```

Go to the end of the file and add these lines (replace with your path for
HEADAS):

```
# Initialize HEASOFT environment
# You must 'heainit' before 'ciao' if you use both heasoft and ciao!
export HEADAS=/home/qw/astro/soft/heasoft-6.25/x86_64-pc-linux-gnu-libc2.23
alias heainit="source $HEADAS/headas-init.sh"
```

Heed the warning about initializing HEASOFT before CIAO. If you don't, you are
almost certain to encounter unpredictable problems where CIAO conflicts with
HEASOFT.

Open a new terminal window so the new `.bashrc` takes effect. Try starting
Xspec:

```
qw@DESKTOP-V1E2O0K:~$ heainit
qw@DESKTOP-V1E2O0K:~$ xspec
Creating a $HOME/.xspec directory for you

                XSPEC version: 12.10.1
        Build Date/Time: Fri May 17 05:44:45 2019

XSPEC12>
```
(To exit Xspec, press `Ctrl + D` once.)


---
